#!/bin/zsh

readonly JGIT_PATH=$HOME/.jgit
readonly JGIT_STATUS=$JGIT_PATH/status.txt
readonly JGIT_CHECKOUT_PATH=$JGIT_PATH/checkout
readonly JGIT_CHECKOUT_LIST=$JGIT_CHECKOUT_PATH/checkoutList.txt
readonly JGIT_SHELVES_PATH=$JGIT_PATH/shelves

jgit_status()
{
	git status > $JGIT_STATUS
	git status
}

jgit_checkout()
{
	mkdir -p $JGIT_CHECKOUT_PATH/$1
	cp $1 $JGIT_CHECKOUT_PATH/$1
	touch JGIT_CHECKOUT_LIST
	echo $1 >> JGIT_CHECKOUT_LIST
}

jgit_reverse_checkout()
{

}

jgit_checkout_list()
{

}

jgit_checkout_list_clear()
{

}

confirm () {
    # call with a prompt string or use a default
    read -q "${1:-Are you sure? [y/N]} " response
    case $response in
        [yY][eE][sS]|[yY]) 
            true
            ;;
        *)
            false
            ;;
    esac
}

#Local shelves
jgit_shelve()
{
	confirm
}

jgit_unshelve()
{

}

jgit_list_shelves()
{

}

jgit_remove_shelve()
{

}

jgit_print_usage()
{
    cat <<- EOF
Usage: jgit [command] <point>

Commands:
	gst | status 			gets status of your git repository
	co | checkout 			checkout file in your git repository
	shelve 				shelf changes in your current git repository
	unshelve 			unshelf changes
	help				Show this extremely helpful text

	-v | --version			Print version
EOF
}

# get opts
args=$(getopt -o gst,co, shelve,help -- $*)

# check if there are arguments
if [[ ($? -ne 0 || $#* -eq 0) ]]
then
	jgit_print_usage

else
# parse rest of options
    for o
    do
        case "$o"
            in
            gst|status)
                jgit_status
                break
                ;;
            co|checkout)
				jgit_checkout $2
				break
				;;
            -h|--help|help)
				jgit_print_usage
				break
				;;
			shelve)
				jgit_shelve
				break
				;;
            --)
                break
                ;;
        esac
    done
fi